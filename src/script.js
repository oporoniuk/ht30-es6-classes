// Теоретичне питання

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування розширює батьківський клас.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Щоб ініціалізувати об'єкт this з батьківському класі


// Завдання

// Створити клас Employee, у якому будуть такі характеристики - 
//name (ім'я), age (вік), salary (зарплата). 
//Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть геттери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee,
// і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть геттер для властивості salary. 
//Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.


// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(newName) {
        newName = newName.trim();
        if (newName === '')
            console.log("Name cannot be empty");

        this._name = newName;
    }



    get age() {
        return this._age;
    }
    set age(newAge) {
        if (newAge <= 0)
            console.log("Invalid age");

        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        if (newSalary <= 0)
            console.log("Invalid value");

        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

const employee1 = new Programmer('Stepanio', 28, 1000, "Spanish");
const employee2 = new Programmer('Antonio', 60, 3450, "Portugues, Japanees");
const employee3 = new Programmer('Alehandro', 52, 4367, "Ukrainian, English, Spanish");
const employee4 = new Programmer('Pedrizio', 37, 2223, "Italian, German");
const employee5 = new Programmer('Maximiminium', 18, 1191, "French, Mandarin, Korean");

console.log('This is employee 1', employee1);
console.log('This is employee 1 salary', employee1.salary)

console.log('This is employee 2', employee2);
console.log('This is employee 2 salary', employee2.salary)

console.log('This is employee 3', employee3);
console.log('This is employee 3 salary', employee3.salary)

console.log('This is employee 4', employee4);
console.log('This is employee 4 salary', employee4.salary)

console.log('This is employee 5', employee5);
console.log('This is employee 5 salary', employee5.salary)